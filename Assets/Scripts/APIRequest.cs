using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class APIRequest : MonoBehaviour
{
    public GameObject targetObject;
    private const string API_URL = "https://963knightmoon369.readyplayer.me/api/texture?prompt=abstract%20colorful%20seamless%20synthwave";

    void Start()
    {
        StartCoroutine(SendAPIRequest());
    }

    /*
 IEnumerator SendAPIRequest()
 {
     // Use JavaScript to send the API request
     string jsCode = @"
     var xhr = new XMLHttpRequest();
     xhr.open('GET', '" + API_URL + @"', true);
     xhr.onreadystatechange = function() {
         if (xhr.readyState === 4 && xhr.status === 200) {
             var response = xhr.responseText;
             var apiData = JSON.parse(response);
             var textureUrl = apiData.data.textureImageUrl;
             var textureRequest = new XMLHttpRequest();
             textureRequest.open('GET', textureUrl, true);
             textureRequest.responseType = 'arraybuffer';
             textureRequest.onreadystatechange = function() {
                 if (textureRequest.readyState === 4 && textureRequest.status === 200) {
                     var arrayBuffer = textureRequest.response;
                     var texture = new Uint8Array(arrayBuffer);
                     OnTextureDownloaded(texture);
                 }
             };
             textureRequest.send();
         }
     };
     xhr.send();";

     Application.ExternalEval(jsCode);
     yield break;
 }

 void OnTextureDownloaded(byte[] data)
 {
     // Convert the byte array to a Texture2D
     Texture2D texture = new Texture2D(1, 1);
     texture.LoadImage(data);

     // Set the texture on the target object's renderer
     Renderer renderer = targetObject.GetComponent<Renderer>();
     renderer.material.mainTexture = texture;
 }


    */


    IEnumerator SendAPIRequest()
    {

        string response = "{\"data\":{\"textureImageUrl\":\"https://oaidalleapiprodscus.blob.core.windows.net/private/org-EX1ExTYWo9a5Hvzl4s6t5lKt/user-uBICWLHlaCUPyd8wKyk0DDAb/img-iq39wfXPQDVout2z0sOWh1N9.png?st=2023-04-19T16%3A41%3A34Z&se=2023-04-19T18%3A41%3A34Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-04-19T16%3A16%3A52Z&ske=2023-04-20T16%3A16%3A52Z&sks=b&skv=2021-08-06&sig=43mBbnDn4EPmT2IGukOjOE/R05Wd%2BK3TSo5bzbGXals%3D\"}}";


        APIData apiData = JsonUtility.FromJson<APIData>(response);

        string textureUrl = apiData.data.textureImageUrl;

        WWW wwwTexture = new WWW(textureUrl);
        yield return wwwTexture;

        if (string.IsNullOrEmpty(wwwTexture.error))
        {
            Texture2D texture = wwwTexture.texture;
            Renderer renderer = targetObject.GetComponent<Renderer>();
            renderer.material.mainTexture = texture;
        }
        else
        {
            Debug.LogError("Error downloading texture: " + wwwTexture.error);
        }

    }



    [System.Serializable]
    public class APIData
    {
        public Data data;
    }

    [System.Serializable]
    public class Data
    {
        public string textureImageUrl;
    }
}
