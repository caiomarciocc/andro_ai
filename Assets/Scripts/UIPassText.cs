﻿using UnityEngine;
using TMPro;

/// <summary>
/// UI view for passing text to JS plugin
/// </summary>
public class UIPassText : MonoBehaviour
{
    // Reference to the input field
    [SerializeField]
    private TMP_InputField input;

    /// <summary>
    /// Passes the text to the JS plugin
    /// </summary>
    public void PassText()
    {
        // If empty, skip it
        if (input.text.Length == 0) return;

        // Get text and pass it to the plugin
        WebGLPluginJS.PassTextParam(input.text);
    }
}
