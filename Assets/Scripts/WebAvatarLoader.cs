using ReadyPlayerMe;
using UnityEngine;
using UnityEngine.UI;

public class WebAvatarLoader : MonoBehaviour
{
    private const string TAG = nameof(WebAvatarLoader);
    private GameObject avatar;
    private string avatarUrl = "";
    public Text RPMURL;
    public MonoBehaviour RPMRuntime;

   

    private void Start()
    {

         
#if !UNITY_EDITOR && UNITY_WEBGL
        PartnerSO partner = Resources.Load<PartnerSO>("Partner");
        
        WebInterface.SetupRpmFrame(partner.Subdomain);
#endif
    }


    private void Update()
    {
        // Check if the function can be called and if the "E" key was pressed
        //     if (canCallFunction && Input.GetKeyDown(KeyCode.E))
        //    {
        // Call the function
        //   CallFunction();
        //  }

        if (Input.GetKeyDown(KeyCode.F))
        {
            // Call the function
           // skinMeshRPM = GameObject.Find("Renderer_Avatar").GetComponent<SkinnedMeshRenderer>();



           


        }

    }
        
        public void OnWebViewAvatarGenerated(string generatedUrl)
    {
        var avatarLoader = new AvatarLoader();
        avatarUrl = generatedUrl;

        string modelId = avatarUrl.Substring(avatarUrl.LastIndexOf("/") + 1, avatarUrl.LastIndexOf(".") - avatarUrl.LastIndexOf("/") - 1);

        Debug.Log(modelId); // Output: "641b5bfb398f7e86e69a9f31"
        avatarUrl = "https://api.readyplayer.me/v1/avatars/" + modelId + ".glb";
        Debug.Log("this is what will be downloaded: " + avatarUrl);



         avatarLoader.OnCompleted += OnAvatarLoadCompleted;
        // avatarLoader.OnFailed += OnAvatarLoadFailed;
        RPMURL.text = avatarUrl;
         // avatarLoader.LoadAvatar(avatarUrl);
        RPMRuntime.SetEnable(true);
       

    }

    private void OnAvatarLoadCompleted(object sender, CompletionEventArgs args)
    {
        if (avatar) Destroy(avatar);
        avatar = args.Avatar;

        Debug.Log("avatar load destroyed");
    }

    private void OnAvatarLoadFailed(object sender, FailureEventArgs args)
    {
       // SDKLogger.Log(TAG,$"Avatar Load failed with error: {args.Message}");
    }
}
