﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// UI view for calling a function from JS plugin
/// </summary>
public class UICallFunction : MonoBehaviour
{
    // Import the JS function
    [DllImport("__Internal")]
    private static extern void CallscenarioStartButton();

    // Import the JS function
    [DllImport("__Internal")]
    private static extern void CallVoiceListButton();

    // The collider that triggers the text popup
    public Collider triggerCollider;

    // The GameObject that displays the text popup
    public TextMeshPro textPopup; 

    // Flag to indicate whether the function can be called or not
    private bool canCallFunction = false;


    public void Start()
    {
        CallVoiceListButtonFunction();

    }

    public void CallVoiceListButtonFunction()
    {
        CallVoiceListButton();
    }


    /// <summary>
    /// Call function in the JS plugin
    /// </summary>
    public void CallFunction()
    {
        // Call the JS function
        CallscenarioStartButton();
    }

    // Called when the trigger collider is entered by a GameObject
    private void OnTriggerEnter(Collider other)
    {
        // Check if the collider that entered has the "Player" tag
        if (other.CompareTag("Animal"))
        {
            // Set the flag to indicate that the function can be called
            canCallFunction = true;

            // Show the text popup
            textPopup.enabled = true;
        }
    }

    // Called when the trigger collider is exited by a GameObject
    private void OnTriggerExit(Collider other)
    {
        // Check if the collider that exited has the "Player" tag
        if (other.CompareTag("Animal"))
        {
            // Reset the flag to indicate that the function cannot be called
            canCallFunction = false;

            // Hide the text popup
            textPopup.enabled = false;
        }
    }

    private void Update()
    {
        // Check if the function can be called and if the "E" key was pressed
   //     if (canCallFunction && Input.GetKeyDown(KeyCode.E))
    //    {
            // Call the function
         //   CallFunction();
      //  }

        if (Input.GetKeyDown(KeyCode.E))
        {
            // Call the function
            CallFunction();
            textPopup.text = "Listening...";    
        }

        if (Input.GetKey(KeyCode.I))
        {
          //  Cursor.lockState = CursorLockMode.None;
          
            
        }

        if (Input.GetKey(KeyCode.O))
        {
           Cursor.lockState = CursorLockMode.Locked;
        }

        if (Input.GetKey(KeyCode.P))
        {
          //  Cursor.lockState = CursorLockMode.Confined;
        }




    }
}
