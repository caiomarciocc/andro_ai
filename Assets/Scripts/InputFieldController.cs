using UnityEngine;
using UnityEngine.UI;

public class InputFieldController : MonoBehaviour
{
    public InputField inputField;
    public Text keyboardText;

    void Start()
    {
        // Register a callback for when the input field value changes
        inputField.onValueChanged.AddListener(OnInputFieldValueChanged);
        inputField.onEndEdit.AddListener(OnPointerClickFun);
        
        
    }

    void OnInputFieldValueChanged(string value)
    {
        // Update the text of the input field with the new value
        inputField.text = value;
        keyboardText.text = value;
        
    }


    public void OnPointerClickFun(string value)
    {
        // Update the text of the input field with the new value
        
        keyboardText.text = "";

    }
}
