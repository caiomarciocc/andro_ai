using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSkybox : MonoBehaviour
{
    public Collider cubeCollider;
    public Material[] skyboxes;
    public Material[] cats;
    public GameObject[] gameObjects;
    public SkinnedMeshRenderer[] skinnedMeshRenderers;

    int currentSkyboxIndex = 0;
    int currentSkinnedMeshRendererIndex = 0;
    int currentGameObjectIndex = 0;
    bool isRendering = true;

    int currentCatIndex = 0;
    int currentCatSkinnedMeshRendererIndex = 0;
    int currentCatGameObjectIndex = 0;
    bool isCatRendering = true;

    public int change = 0;
    public SkinnedMeshRenderer publicMesh;
    public SkinnedMeshRenderer catMesh;
    public GameObject publicTrump;
    public

    void Start()
    {
        RenderSettings.skybox = skyboxes[currentSkyboxIndex];
       // catMesh.material = cats[currentCatIndex];
     //   CopySkinnedMeshRendererValues(skinnedMeshRenderers[currentSkinnedMeshRendererIndex], GetComponent<SkinnedMeshRenderer>());
      //  currentSkinnedMeshRendererIndex = 1;
    }



    void OnTriggerEnter(Collider other)
    {
        if (other == cubeCollider)
        {
            //    ChangeMySkybox(); // call the ChangeMySkybox method when the player collides with the cube
        }
    }


    public void turnoffTrump()
    {
        publicTrump.SetActive(false);
    }

    public void turnoffRPM()
    {
        publicMesh.enabled = false;
    }

    public void Change()
    {

        change++;
        if (change == 1)


        {
            publicMesh.enabled = true;
            publicTrump.SetActive(false);
        }


        if (change == 2)


        {
            publicMesh.enabled = true;
            publicTrump.SetActive(false);
        }

        if (change == 3)


        {
            publicMesh.enabled = true;
            publicTrump.SetActive(false);
        }

        if (change == 5)


        {
            publicMesh.enabled = true;
            publicTrump.SetActive(false);
        }

        if (change == 4)

        {
            publicMesh.enabled = false;
            publicTrump.SetActive(true);
            change = 0;
        }




    }


    public void CycleThroughMeshAndGameObjects()
    {
        if (isRendering)
        {
            CycleSkinnedMeshRenderer();
            if (!isRendering && currentGameObjectIndex + 1 >= gameObjects.Length && currentSkinnedMeshRendererIndex >= skinnedMeshRenderers.Length)
            // all SkinnedMeshRenderer objects have been cycled through and game objects have not yet started
            {
                isRendering = true; // reset isRendering to true

                ToggleGameObject();
                Debug.Log("hi");
            }
        }
        else
        {
            Debug.Log("about to run this TOGGLEGAMEBOJET");


        }
    }

    public void ChangeMyCat()
    {
        currentCatIndex++;
        if (currentCatIndex >= cats.Length)
        {
            currentCatIndex = 0;
        }
        catMesh.material = cats[currentCatIndex];
    }


    public void ChangeMySkybox()
    {
        currentSkyboxIndex++;
        if (currentSkyboxIndex >= skyboxes.Length)
        {
            currentSkyboxIndex = 0;
        }
        RenderSettings.skybox = skyboxes[currentSkyboxIndex];
    }

    public void ChangeMySkyboxLess()
    {
        currentSkyboxIndex--;
        if (currentSkyboxIndex < 0)
        {
            currentSkyboxIndex = skyboxes.Length - 1;
        }
        RenderSettings.skybox = skyboxes[currentSkyboxIndex];
    }

    public void ToggleGameObject()
    {
        if (currentGameObjectIndex < gameObjects.Length)
        {
            if (currentGameObjectIndex > 0)
            {
                gameObjects[currentGameObjectIndex - 1].SetActive(false); // disable the previously activated GameObject
            }

            if (currentSkinnedMeshRendererIndex > 0)
            {
                skinnedMeshRenderers[currentSkinnedMeshRendererIndex - 1].enabled = false; // disable the previously activated SkinnedMeshRenderer
            }

            if (currentSkinnedMeshRendererIndex < skinnedMeshRenderers.Length)
            {
                skinnedMeshRenderers[currentSkinnedMeshRendererIndex].enabled = false; // disable the current SkinnedMeshRenderer
            }

            gameObjects[currentGameObjectIndex].SetActive(true); // activate the current GameObject

            currentGameObjectIndex++;
        }
        else if (currentSkinnedMeshRendererIndex < skinnedMeshRenderers.Length)
        {
            if (currentSkinnedMeshRendererIndex > 0)
            {
                skinnedMeshRenderers[currentSkinnedMeshRendererIndex - 1].enabled = false; // disable the previously activated SkinnedMeshRenderer
            }

            CopySkinnedMeshRendererValues(skinnedMeshRenderers[currentSkinnedMeshRendererIndex], GetComponent<SkinnedMeshRenderer>());

            if (currentGameObjectIndex > 0 && currentGameObjectIndex <= gameObjects.Length)
            {
                gameObjects[currentGameObjectIndex - 1].SetActive(false); // disable the previously activated GameObject
            }

            skinnedMeshRenderers[currentSkinnedMeshRendererIndex].enabled = true; // activate the current SkinnedMeshRenderer

            currentSkinnedMeshRendererIndex++;
        }
        else
        {
            currentGameObjectIndex = 0;
            currentSkinnedMeshRendererIndex = 0;

            gameObjects[gameObjects.Length - 1].SetActive(false); // disable the last activated GameObject
            skinnedMeshRenderers[skinnedMeshRenderers.Length - 1].enabled = false; // disable the last activated SkinnedMeshRenderer

            isRendering = true;
        }
    }






    public void CycleSkinnedMeshRenderer()
    {
        if (currentSkinnedMeshRendererIndex < skinnedMeshRenderers.Length)
        {
            if (currentSkinnedMeshRendererIndex > 1)
            {
                skinnedMeshRenderers[currentSkinnedMeshRendererIndex - 1].enabled = false;
            }
            CopySkinnedMeshRendererValues(skinnedMeshRenderers[currentSkinnedMeshRendererIndex], GetComponent<SkinnedMeshRenderer>());
            skinnedMeshRenderers[currentSkinnedMeshRendererIndex].enabled = true;
            currentSkinnedMeshRendererIndex++;
        }
        else
        {
            isRendering = false;
        }
    }

    private void CopySkinnedMeshRendererValues(SkinnedMeshRenderer from, SkinnedMeshRenderer to)
    {
        to.sharedMesh = from.sharedMesh;
        to.rootBone = from.rootBone;
        to.bones = from.bones;
        to.materials = from.materials;
    }

    public void AddSkinnedMeshRenderer(SkinnedMeshRenderer renderer)
    {
        // Create a new array with space for an additional SkinnedMeshRenderer
        SkinnedMeshRenderer[] newRenderers = new SkinnedMeshRenderer[skinnedMeshRenderers.Length + 1];

        // Copy the contents of the existing array to the new array
        for (int i = 0; i < skinnedMeshRenderers.Length; i++)
        {
            newRenderers[i] = skinnedMeshRenderers[i];
        }

        // Add the new SkinnedMeshRenderer to the end of the new array
        newRenderers[newRenderers.Length - 1] = renderer;

        // Update the reference to the array of SkinnedMeshRenderers
        skinnedMeshRenderers = newRenderers;
    }

}



