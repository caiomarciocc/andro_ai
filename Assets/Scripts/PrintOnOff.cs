// Implement OnDisable and OnEnable script functions.
// These functions will be called when the attached GameObject
// is toggled.
// This example also supports the Editor.  The Update function
// will be called, for example, when the position of the
// GameObject is changed.

using GoogleSheetsForUnity;
using UnityEngine;

[ExecuteInEditMode]
public class PrintOnOff : MonoBehaviour
{
    public GameObject OnOff;

    public GameObject OnOffComponent;
    public void OnOffHuman()
    {

        OnOff.SetActive(false);
        OnOff.SetActive(true);
        // GameObject.Find("Human Base 33").gameObject.SetActive(false);
        // GameObject.Find("Human Base 33").gameObject.SetActive(true);
        Debug.Log("hii");
    }

    public void Start()
    {
        // call OnOffComponentVoid method every 20 seconds
        //InvokeRepeating("OnComponentVoid", 0, 20);
      
    }

    public void OnComponentVoid()
    {

        OnOffComponent.gameObject.GetComponent<FilesExample>().enabled = true;
       // Invoke("DisableComponent", 9f);
        Debug.Log("hii");



        //OnOffComponent.gameObject.GetComponent<FilesExample>().enabled = true;
        //OnOffComponent.gameObject.GetComponent<FilesExample>().enabled = false;
        //OnOff.SetActive(true);
        // GameObject.Find("Human Base 33").gameObject.SetActive(false);
        // GameObject.Find("Human Base 33").gameObject.SetActive(true);


        Debug.Log("hii2");
    }

    public void OffComponentVoid()
    {
      //  OnOffComponent.gameObject.GetComponent<FilesExample>().enabled = true;
        //OnOffComponent.gameObject.GetComponent<FilesExample>().enabled = false;
        //OnOff.SetActive(true);
        // GameObject.Find("Human Base 33").gameObject.SetActive(false);
        // GameObject.Find("Human Base 33").gameObject.SetActive(true);
        Debug.Log("hii");
    }


    private void DisableComponent()
    {
        OnOffComponent.gameObject.GetComponent<FilesExample>().enabled = false;
    }



    void OnDisable()
    {
       // Debug.Log("PrintOnDisable: script was disabled");
    }

    void OnEnable()
    {
       // Debug.Log("PrintOnEnable: script was enabled");
    }

    void Update()
    {
#if UNITY_EDITOR
      //  Debug.Log("Editor causes this Update");
#endif
    }
}