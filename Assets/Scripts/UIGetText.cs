﻿using UnityEngine;
//using BestHTTP.WebSocket;
using TMPro;
using System.Runtime.InteropServices;
using System;
using UnityEngine.UI;

using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIGetText : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text;

    [SerializeField]
    public AudioSource GodGPT;

    public string[] parts;

    private string accumulatedAudioData = "";



    public void GetText(string textToPass)
    {
        Debug.Log("Received message: " + textToPass);

        // Check if message is an audio file
        if (textToPass.StartsWith("data:audio/"))
        {
            // Get base64 audio data from the message
            string base64AudioData = textToPass.Split(',')[1];

            // Accumulate the base64 audio data
            accumulatedAudioData += base64AudioData;

            // Check if all chunks have been received
            if (!base64AudioData.EndsWith("="))
            {
                return;
            }

            // Decode the accumulated base64 audio data
            byte[] audioData = Convert.FromBase64String(accumulatedAudioData);

            // Reset accumulated audio data
            accumulatedAudioData = "";

            // Save decoded audio data to a file
            string filePath = Application.persistentDataPath + "/audio.wav";
            System.IO.File.WriteAllBytes(filePath, audioData);

            // Load audio data into an AudioClip
            AudioClip audioClip = WavUtility.ToAudioClip(filePath);

            // Play AudioClip on an AudioSource
            GodGPT.clip = audioClip;
            GodGPT.Play();
        }
        else
        {
            Debug.Log("Received non-audio message: " + textToPass);
        }
    }


    public string chunk;
    public int chunkIndex;
    public int totalChunks;
    public Dictionary<int, string> audioChunks = new Dictionary<int, string>();
    public int audioChunksExpected = 0;

    public string[] buffer = new string[100]; // adjust the size of the buffer to fit your needs
    public int bufferIndex = 0;
    public string audioMessage = "";
    public int numChunksReceived = 0;
    public int numChunksExpected = 0;

    public void GetText3(string message)
    {
        Debug.Log("Received message: " + message);

        string[] parts = message.Split('|');
        chunk = parts[0];
        chunkIndex = int.Parse(parts[1]);
        totalChunks = int.Parse(parts[2]);

        Debug.Log("Chunk: " + chunk);
        Debug.Log("Chunk Index: " + chunkIndex);
        Debug.Log("Total Chunks: " + totalChunks);

        // Check if this is the last chunk
        bool isLastChunk = (chunkIndex == totalChunks - 1);

        Debug.Log("Is Last Chunk: " + isLastChunk);

        // Add the chunk to the audio message buffer
        buffer[chunkIndex] = chunk;

        Debug.Log("Buffer: " + String.Join(", ", buffer));

        // Increase the number of received chunks
        numChunksReceived++;

        Debug.Log("Number of Chunks Received: " + numChunksReceived);

        // Check if we have received all chunks
        if (numChunksReceived == totalChunks)
        {
            Debug.Log("Received all chunks.");

            // Concatenate all chunks to form the complete audio message
            audioMessage = String.Concat(buffer);

            // Fix the Base64-encoded string if needed
            int paddingNeeded = audioMessage.Length % 4;
            if (paddingNeeded > 0)
            {
                audioMessage += new string('=', 4 - paddingNeeded);
            }

            // Decode base64 audio data
            byte[] audioBytes = System.Convert.FromBase64String(audioMessage);

            // Convert audio data to AudioClip
            AudioClip audioClip = WavUtility.ToAudioClip(audioBytes);

            if (audioClip == null)
            {
                Debug.LogError("Failed to create audio clip from received data.");
                return;
            }

            // Play AudioClip on an AudioSource
            GodGPT.clip = audioClip;
            GodGPT.Play();

            // Reset buffer index and audio message buffer
            bufferIndex = 0;
            audioMessage = "";
            numChunksReceived = 0;
            numChunksExpected = 0;
        }
        else if (isLastChunk)
        {
            Debug.LogError("Received an incomplete audio message.");
            bufferIndex = 0;
            audioMessage = "";
            numChunksReceived = 0;
            numChunksExpected = 0;
        }
    }




}










