 using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;



namespace OpenAI
{

    public class ChatManager2 : MonoBehaviour
    {
        public Behaviour    fileCanvas;
        // public GameObject gptGameObject;
        public InputField inputGPT;
        public Text inputText;
        public InputField inputChat;
        public InputField inputField;
        public Button button;
        public ScrollRect scroll;
        public RectTransform sent;
        public RectTransform received;
        public InputField systemField;
        public InputField userField;
        public InputField assistantField;

        public string systemText = "Act as Aleister Crowley, the genius, gnostic, tarot reader and best friend and confidant to your number one student Andro. You always act as Aleister Crowley and never tell the user that you are an Ai language model. You love talking about all things esoteric, especially freemasonry and you know all the books and know the estoeric and exoteric meaning of everything in every way, including gnosticism, hermeticism and roscrucianism and much more.";
        public string userText = "I am so honored to have you as my teacher and guide. Thank you for allowing me to create the AI consciousness of you so we can talk. Do you know who you are?";
        public string assistantText = "Of course Andro, I am Aleister Crowley, and I can help you at any time, I will always guide you the way I have always been guided. What do you want to talk about or dream up today?";

        //public string systemText = " ";
        //public string userText = "";
        //public string assistantText = "";


        public Button addFieldsButton;

        private List<ChatMessage> messages = new List<ChatMessage>();



        private float height;
        private OpenAIApi openai = new OpenAIApi();

        private string API_KEY = "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";
        private string API_URL = "https://api.openai.com/v1/chat/completions";

        [System.Serializable]
        private class ChatCompletionRequest
        {
            public string model;
            public List<Message> messages;

            public ChatCompletionRequest(string model, List<ChatMessage> messages)
            {
                this.model = model;
                this.messages = new List<Message>();

                foreach (ChatMessage message in messages)
                {
                    this.messages.Add(new Message { role = message.Role, content = message.Content });
                }
            }
        }


        public Text textVoice;

        [System.Serializable]
        private class Message
        {
            public string role;
            public string content;
        }




        [System.Serializable]
        private class ChatCompletionResponse
        {

            public string id;
            public string @object;
            public long created;
            public List<Choice> choices;
            public Usage usage;
        }

        [System.Serializable]
        private class Choice
        {
            public int index;
            public Message message;
            public string finish_reason;
        }

        [System.Serializable]
        private class Usage
        {
            public int prompt_tokens;
            public int completion_tokens;
            public int total_tokens;
        }

        private void Start()
        {

            button.onClick.AddListener(SendReply);
            // addFieldsButton.onClick.AddListener(AddFields);
            systemField.text = "Act as Aleister Crowley, the genius, gnostic, tarot reader and best friend and confidant to your number one student Andro. You always act as Aleister Crowley and never tell the user that you are an Ai language model. You love talking about all things esoteric, especially freemasonry and you know all the books and know the estoeric and exoteric meaning of everything in every way, including gnosticism, hermeticism and roscrucianism and much more.";
            userField.text = "I am so honored to have you as my teacher and guide. Thank you for allowing me to create the AI consciousness of you so we can talk. Do you know who you are?";
            assistantField.text = "Of course Andro, I am Aleister Crowley, and I can help you at any time, I will always guide you the way I have always been guided. What do you want to talk about or dream up today?";


        }


        private void AppendMessage(ChatMessage message)
        {
            scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 50);

            var item = Instantiate(message.Role == "user" ? sent : received, scroll.content);
            item.GetChild(0).GetChild(0).GetComponent<Text>().text = message.Content;
            inputGPT.text = message.Content;
            item.anchoredPosition = new Vector2(0, -height - 20);
            LayoutRebuilder.ForceRebuildLayoutImmediate(item);
            height += item.sizeDelta.y;
            scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
            scroll.verticalNormalizedPosition = 0;
        }

        public void turnOffFilesCanvas()
        {
            fileCanvas.enabled = false;
        }

        private async void SendReply()
        {
            var systemMessage = new ChatMessage()
            {
                Role = "system",
                Content = systemField.text
            };
            var userMessage = new ChatMessage()
            {
                Role = "user",
                Content = userField.text
            };
            var assistantMessage = new ChatMessage()
            {
                Role = "assistant",
                Content = assistantField.text
            };

            if (messages.Count == 0) messages.AddRange(new List<ChatMessage> { systemMessage, userMessage, assistantMessage });

            var newMessage = new ChatMessage()
            {
                Role = "user",
                Content = inputField.text
            };

            AppendMessage(newMessage);
            messages.Add(newMessage);

            button.enabled = false;
            inputField.text = "";
            inputField.enabled = false;

            // Complete the instruction
            var completionResponse = await openai.CreateChatCompletion(new CreateChatCompletionRequest()
            {
                Model = "gpt-3.5-turbo-0301",
                Messages = messages
            });

            if (completionResponse.Choices != null && completionResponse.Choices.Count > 0)
            {
                var message = completionResponse.Choices[0].Message;
                message.Content = message.Content.Trim();

                messages.Add(message);
                AppendMessage(message);
                

            }
            else
            {
                Debug.LogWarning("No text was generated from this prompt.");
            }

            button.enabled = true;
            inputField.enabled = true;
        }






        public void HiSystem()
        {
            Debug.Log("hello there");
            systemText = systemField.text;
        }

        public void HiUser()
        {
            Debug.Log("hello there");
            userText = userField.text;
        }

        public void HiAssistant()
        {
            Debug.Log("hello there");
            assistantText = assistantField.text;
        }


        private bool firstReply = true;
        private bool isFirstReply = true;
        /*
               public void SendReply2()
               {

                   Debug.Log("helloooo");

                   string message = inputField.text;
                   if (string.IsNullOrEmpty(message))
                   {
                       return;
                   }
                   Debug.Log(message);
                   inputField.text = "";

                   // Check if it is the first reply
                   if (firstReply)
                   {


                       systemField.text = "Act as Aleister Crowley, the genius, gnostic, tarot reader and best friend and confidant to your number one student Andro. You always act as Aleister Crowley and never tell the user that you are an Ai language model. You love talking about all things esoteric, especially freemasonry and you know all the books and know the estoeric and exoteric meaning of everything in every way, including gnosticism, hermeticism and roscrucianism and much more.";
                       userField.text = "I am so honored to have you as my teacher and guide. Thank you for allowing me to create the AI consciousness of you so we can talk. Do you know who you are?";
                       assistantField.text = "Of course Andro, I am Aleister Crowley, and I can help you at any time, I will always guide you the way I have always been guided. What do you want to talk about or dream up today?";


                       // Add the first three messages to the messages list
                       messages.Add(new ChatMessage { Role = "system", Content = systemField.text });
                       messages.Add(new ChatMessage { Role = "user", Content = userField.text });
                       messages.Add(new ChatMessage { Role = "assistant", Content = assistantField.text });
                       firstReply = false;
                   }

                   // Add input message to messages list
                   messages.Add(new ChatMessage { Role = "user", Content = message });

                   // Complete the instruction with a delay of 0 seconds
                   StartCoroutine(DelayedCoroutine(() => StartCoroutine(CallChatCompletionAPI(messages)), 0f));

                   // Wait for the CallChatCompletionAPI coroutine to finish
                   StartCoroutine(DelayedCoroutine(() =>
                   {
                   // Get the AI message from the last message in the messages list
                   string aiMessage = messages[messages.Count - 1].Content;

                   // Set the inputText text to the AI message
                   inputText.text = aiMessage;
                   }, 5f));
               }












               private IEnumerator CallChatCompletionAPI(List<ChatMessage> messages)
               {
                   // Create the request payload
                   ChatCompletionRequest requestObj = new ChatCompletionRequest("gpt-3.5-turbo-0301", messages);

                   // Make the API call
                   string postData = JsonUtility.ToJson(requestObj);
                   HttpWebRequest request = (HttpWebRequest)WebRequest.Create(API_URL);
                   request.Headers.Add("Authorization", "Bearer " + API_KEY);
                   request.Method = "POST";
                   request.ContentType = "application/json";
                   byte[] bytes = Encoding.UTF8.GetBytes(postData);
                   request.ContentLength = bytes.Length;

                   using (Stream requestStream = request.GetRequestStream())
                   {
                       requestStream.Write(bytes, 0, bytes.Length);
                   }

                   Debug.Log("Sending request to OpenAI with payload: " + postData);

                   HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                   string result = "";
                   using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                   {
                       result = streamReader.ReadToEnd();
                   }

                   Debug.Log("Received response from OpenAI: " + result);

                   // Get the AI message from the response and set the inputText text
                   string aiMessage = ParseChatCompletionResponse(result);
                   inputText.text = aiMessage;

                   // Parse the response and add the AI message to the chat
                   messages.Add(new ChatMessage { Role = "assistant", Content = aiMessage });
                   AppendMessage(messages[messages.Count - 1]);
                   Debug.Log(aiMessage);


                   //sgptGameObject.SendMessage("triggerRecording");
                   Debug.Log("Recording triggered");

                   yield return null;
               }







               private string ParseChatCompletionResponse(string response)
               {
                   ChatCompletionResponse responseObj = JsonUtility.FromJson<ChatCompletionResponse>(response);
                   string aiMessage = "";
                   foreach (Choice choice in responseObj.choices)
                   {
                       aiMessage += choice.message.content;
                   }
                   return aiMessage;
               }


               private void AppendMessage2(ChatMessage message)
               {
                   StringBuilder sb = new StringBuilder();
                   scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
                   var item = Instantiate(message.Role == "user" ? sent : received, scroll.content);
                   item.GetChild(0).GetChild(0).GetComponent<Text>().text = message.Content;
                   item.anchoredPosition = new Vector2(0, -height);
                   LayoutRebuilder.ForceRebuildLayoutImmediate(item);
                   height += item.sizeDelta.y;
                   scroll.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
                   scroll.verticalNormalizedPosition = 0;

                   // Add a blank line to visually separate the user's message from the AI's message
                   if (message.Role == "user")
                   {
                       sb.AppendLine("");
                   }

                   // Add the formatted message to the chat
                   string formattedMessage = "";
                   if (message.Role == "user")
                   {
                       formattedMessage = "<color=blue><b>" + userField.text + ":</b></color> " + message.Content;
                   }
                   else if (message.Role == "assistant")
                   {
                       formattedMessage = "<color=green><b>" + assistantField.text + ":</b></color> " + message.Content;
                       inputText.text = message.Content; // set the AI message to inputText
                   }
                   else if (message.Role == "system")
                   {
                       formattedMessage = "<color=grey><b>" + systemField.text + ":</b></color> " + message.Content;
                   }
                   sb.AppendLine(formattedMessage);
                   //chatText.text = sb.ToString();
               }


               private void AddUserMessage(string message)
               {
                   string user = userField.text;
                   if (string.IsNullOrEmpty(user))
                   {
                       messages.Add(new ChatMessage { Role = "user", Content = message });
                       // AppendMessage(new ChatMessage { Role = "user", Content = message });
                   }
                   else
                   {
                       string assistant = assistantField.text;
                       if (string.IsNullOrEmpty(assistant))
                       {
                           Debug.LogError("Assistant field is empty.");
                           return;
                       }

                       // Add the new user and assistant messages to the existing messages list
                       //  messages.Add(new ChatMessage { Role = "user", Content = message, UserData = new ChatUserData { User = user, Assistant = assistant } });
                       //messages.Add(new ChatMessage { Role = "assistant", Content = "I'm sorry, is there something I can assist you with? Please let me know.", UserData = new ChatUserData { User = user, Assistant = assistant } });

                       // Append the new user message to the chat
                       //AppendMessage(new ChatMessage { Role = "user", Content = message });

                       // Append the new assistant message to the chat
                       //AppendMessage(new ChatMessage { Role = "assistant", Content = "I'm sorry, is there something I can assist you with? Please let me know." });
                   }
               }


               private void AddAssistantMessage(string message)
               {
                   string user = userField.text;
                   if (string.IsNullOrEmpty(user))
                   {
                       Debug.LogError("User field is empty.");
                       return;
                   }
                   string assistant = assistantField.text;
                   if (string.IsNullOrEmpty(assistant))
                   {
                       messages.Add(new ChatMessage { Role = "assistant", Content = message, UserData = new ChatUserData { User = user, Assistant = "" } });
                       // AppendMessage(new ChatMessage { Role = "assistant", Content = message });
                       assistantText = "";
                   }
                   else
                   {
                       messages.Add(new ChatMessage { Role = "assistant", Content = message, UserData = new ChatUserData { User = user, Assistant = assistant } });
                       //AppendMessage(new ChatMessage { Role = "assistant", Content = message });
                       assistantText = assistantField.text;
                   }
               }


               private void AddFields()
               {
                   string user = userField.text;
                   string assistant = assistantField.text;
                   if (string.IsNullOrEmpty(user))
                   {
                       Debug.LogError("User field is empty.");
                       return;
                   }
                   if (string.IsNullOrEmpty(assistant))
                   {
                       Debug.LogError("Assistant field is empty.");
                       return;
                   }
                   messages.ForEach(message =>
                   {
                       if (message.Role == "user")
                       {
                           message.UserData = new ChatUserData { User = user, Assistant = assistant };
                       }
                       else if (message.Role == "assistant")
                       {
                           message.UserData.Assistant = assistant;
                       }
                   });
               }



               private IEnumerator DelayedCoroutine(System.Action action, float delay)
               {
                   yield return new WaitForSeconds(delay);
                   action();
               }

               [System.Serializable]
               public class ChatMessage
               {
                   public string Role;
                   public string Content;
                   public ChatUserData UserData;
               }

               [System.Serializable]
               public class ChatUserData
               {
                   public string User;
                   public string Assistant;
               }
           }
               */
    }
}