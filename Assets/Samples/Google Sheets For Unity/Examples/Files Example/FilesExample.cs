﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.Networking;
using System.Collections;
using System.Net;
using System.Text;

namespace GoogleSheetsForUnity
{
    public class FilesExample : MonoBehaviour
    {

        private string _folderID = "1AvANTaEpGV1asFZgE4B_29xQchrItrd";

        public GameObject targetObject;
        public GameObject scripts;
        public GameObject myPlane;
        public GameObject myPlane2;
        public GameObject myPlane3;
        public GameObject myPlane4;
        private string _filePath;
        private string _cloudFileID;
        private string _binaryFileName = "PlayerInfo.bin";
        private string _textFileName = "PlayerInfo.txt";
        private PlayerInfo _playerData;

        // Simple struct for the example.
        [System.Serializable]
        public struct PlayerInfo
        {
            public string name;
            public int level;
            // public float health;
            public string health;
            public string role;
        }





        private void OnEnable()
        {

            
            // Subscribe for catching cloud responses.
            Drive.responseCallback += HandleDriveResponse;
        }

        private void OnDisable()
        {
            Drive.responseCallback -= HandleDriveResponse;
        }

        private void Start()
        {
            _filePath = Application.dataPath + "/Google Sheets For Unity/Examples/Files Example/";
            _cloudFileID = "1t7Ayxf3B6ZYKaNg-nCUHqIYsTNtay6vJ";
            _playerData = new PlayerInfo()
            {
                name = "Mithrandir",
                level = 99,
                // health = 98.6f,
                health = "1234",
                role = "Wizzard",
            };
            getText(_cloudFileID);
            //  GetTextureJson2();

            // Invoke the Start function every 20 seconds.
            // InvokeRepeating("Start", 10f, 5f);
        }


        private void OnGUI()
        {
            GUILayout.Space(10f);
            GUILayout.BeginHorizontal();
            GUILayout.Space(10f);
            GUILayout.Label("This example will show how to create or retrieve files from Google Drive." +
                "This example does not cover further posibilities of the API such as deleting files, and creating or deleting folders.", GUILayout.MaxWidth(600f));
            GUILayout.EndHorizontal();
            GUILayout.Space(10f);
            GUILayout.BeginVertical("Example 'Player' Object Data:", GUI.skin.box, GUILayout.MaxWidth(230));
            GUILayout.Space(20f);

            GUILayout.BeginHorizontal();
            GUILayout.Space(10f);
            GUILayout.Label("Player Name:", GUILayout.MaxWidth(100f));
            _playerData.name = GUILayout.TextField(_playerData.name, GUILayout.MaxWidth(100f));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(10f);
            GUILayout.Label("Player Level:", GUILayout.MaxWidth(100f));
            _playerData.level = int.Parse(GUILayout.TextField(_playerData.level.ToString(), GUILayout.MaxWidth(100f)));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(10f);
            GUILayout.Label("Player Health:", GUILayout.MaxWidth(100f));
            //  _playerData.health = float.Parse(GUILayout.TextField(_playerData.health.ToString(), GUILayout.MaxWidth(100f)));
            _playerData.health = GUILayout.TextField(_playerData.health, GUILayout.MaxWidth(100f));
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Space(10f);
            GUILayout.Label("Player Role:", GUILayout.MaxWidth(100f));
            _playerData.role = GUILayout.TextField(_playerData.role, GUILayout.MaxWidth(100f));
            GUILayout.EndHorizontal();
            GUILayout.Space(5f);
            GUILayout.EndVertical();

            GUILayout.BeginArea(new Rect(0, 200, 600, 1000));
            GUILayout.BeginHorizontal();
            GUILayout.Space(10f);
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Save data to local binary File", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            {
                SaveLocalBinaryFile(_filePath + _binaryFileName);
            }

            if (GUILayout.Button("Save data to local text File", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            {
                SaveLocalTextFile(_filePath + _textFileName);
            }

            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Load data from local Binary File", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            {
                LoadLocalBinaryFile(_filePath + _binaryFileName);
            }

            if (GUILayout.Button("Load data from local Text File", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            {
                LoadLocalTextFile(_filePath + _textFileName);
            }

            GUILayout.EndHorizontal();

            GUILayout.Space(10f);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Save Data to Cloud as Binary File", GUILayout.MaxWidth(220f)))
            {
                UploadBinaryFile();
            }

            if (GUILayout.Button("Save Data to Cloud as Text File", GUILayout.MaxWidth(220f)))
            {
                UploadTextFile();
            }

            GUILayout.EndHorizontal();
            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Google Drive file id:", GUILayout.MaxWidth(120f));
            _cloudFileID = GUILayout.TextField(_cloudFileID, GUILayout.MaxWidth(220f));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Get Binary File From Cloud", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            {
                if (string.IsNullOrEmpty(_cloudFileID))
                    Debug.Log("Cannot retrieve a file: please provide an id for the file on Google Drive.");
                else
                    Drive.GetBinaryFile(_cloudFileID);
            }
            if (GUILayout.Button("Get Text File From Cloud", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            {
                _cloudFileID = "1t7Ayxf3B6ZYKaNg-nCUHqIYsTNtay6vJ";

                if (string.IsNullOrEmpty(_cloudFileID))
                    //Debug.Log("Cannot retrieve a file: please provide an id for the file on Google Drive.");
                  
                    Drive.GetTextFile(_cloudFileID);
                else
                    Drive.GetTextFile(_cloudFileID);
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        private void SaveLocalTextFile(string filePath)
        {
            string jsonPlayer = JsonUtility.ToJson(_playerData);
            File.WriteAllText(filePath, jsonPlayer, System.Text.Encoding.UTF8);

            Debug.Log("Data saved locally as a text file.");
        }

        private void LoadLocalTextFile(string filePath)
        {
            if (!File.Exists(filePath))
                return;

            string fileData = File.ReadAllText(filePath, System.Text.Encoding.UTF8);
            _playerData = JsonUtility.FromJson<PlayerInfo>(fileData);

            OutputData("local text file");
        }

        private void SaveLocalBinaryFile(string filePath)
        {
            FileStream fileStream = File.Open(filePath, FileMode.CreateNew);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, _playerData);
            fileStream.Flush();

            Debug.Log("Data saved locally as a binary file.");
        }

        private void LoadLocalBinaryFile(string filePath)
        {
            if (!File.Exists(filePath))
                return;

            FileStream fileStream = File.Open(filePath, FileMode.Open);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            _playerData = (PlayerInfo)binaryFormatter.Deserialize(fileStream);

            OutputData("local binary file");
        }

        public void getText(string get)
        {
            Drive.GetTextFile(_cloudFileID);
        }


        private void HandleDriveResponse(Drive.DataContainer dataContainer)
        {
            if (dataContainer.QueryType == Drive.QueryType.createBinaryFile)
            {
                Debug.LogFormat("Binary file created with name {0} and id {1}, at folder {2}.", dataContainer.fileName, dataContainer.fileId, dataContainer.folderName);
            }

            if (dataContainer.QueryType == Drive.QueryType.createTextFile)
            {
                Debug.LogFormat("Text file created with name {0} and id {1}, at folder {2}.", dataContainer.fileName, dataContainer.fileId, dataContainer.folderName);
            }

            if (dataContainer.QueryType == Drive.QueryType.getBinaryFile)
            {
                Debug.Log(dataContainer.msg);

                byte[] decodedBytes = Convert.FromBase64String(dataContainer.payload);
                MemoryStream memStream = new MemoryStream(decodedBytes);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                _playerData = (PlayerInfo)binaryFormatter.Deserialize(memStream);
                OutputData("Drive binary file");
            }

            if (dataContainer.QueryType == Drive.QueryType.getTextFile)
            {
                Debug.Log(dataContainer.msg);
                _playerData = JsonUtility.FromJson<PlayerInfo>(dataContainer.payload);
                OutputData("Drive text file");
            }
        }

        private void UploadBinaryFile()
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter format = new BinaryFormatter();
            format.Serialize(memStream, _playerData);

            Drive.CreateBinaryFile(memStream.ToArray(), _binaryFileName);
        }

        private void UploadTextFile()
        {
            string jsonPlayer = JsonUtility.ToJson(_playerData);

            Drive.CreateTextFile(jsonPlayer, _textFileName);
        }

        private void OutputData(string source)
        {
           // Debug.Log("<color=yellow>Object data retrieved from " + source + ":\n</color>" +
              //              "Name: " + _playerData.name + "\n" +
              //              "Level: " + _playerData.level + "\n" +
              //              "Health: " + _playerData.health + "\n" +
              //              "Role: " + _playerData.role + "\n");

            // Drive.GetImageFile(_playerData.name);
            scripts.SendMessage("getImage", _playerData.name);
            //byte[] textureBytes = Convert.FromBase64String(_playerData.health);

            // Create a texture from the byte array
            //Texture2D texture = new Texture2D(1, 1);

            //texture.LoadImage(textureBytes);
           // Debug.Log("it worked " + _playerData.health);
            // Assign the texture to the material of the target object
           // Renderer renderer = targetObject.GetComponent<Renderer>();
           // renderer.material.mainTexture = texture;

            // Make the texture readable by setting its texture format to RGBA32
           // texture.Apply(true);

        }


        private Texture2D ScaleTexture(Texture2D texture, int width, int height)
        {
            // Create a new texture with the desired size
            Texture2D scaledTexture = new Texture2D(width, height);

            // Scale the texture data to fit the new texture size
            Color[] pixels = scaledTexture.GetPixels(0);
            Color[] originalPixels = texture.GetPixels(0);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int originalX = Mathf.RoundToInt((float)x / width * texture.width);
                    int originalY = Mathf.RoundToInt((float)y / height * texture.height);
                    pixels[y * width + x] = originalPixels[originalY * texture.width + originalX];
                }
            }
            scaledTexture.SetPixels(pixels, 0);

            // Apply changes to the texture and return it
            scaledTexture.Apply();
            return scaledTexture;
        }



        public void RunGetTextureJson2()
        {
         StartCoroutine(GetTextureJson2());
            Debug.Log("hi333");
        }

        public void RunGetTextureJson()
        {
            StartCoroutine(GetTextureJson());
            Debug.Log("hi333");
        }



        public IEnumerator GetTextureJson2()
        {
            string url = "https://script.google.com/macros/s/AKfycbzxCoa-8g8Qxx9Q8PpX1c-hxJogGco2-9099qwFOmwXNw5zWUxp_m0h1X0UYWyQQw3_/exec"; // replace with your web app URL
            string data = "";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close(); 

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string jsonResponse = reader.ReadToEnd();

            Debug.Log(jsonResponse);

            yield break;
        }



        public IEnumerator GetTextureJson()
        {
            string url = "https://drive.google.com/uc?id=1t7Ayxf3B6ZYKaNg-nCUHqIYsTNtay6vJ";
            UnityWebRequest www = UnityWebRequest.Get(url);
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Error retrieving texture.json: " + www.error);
            }
            else
            {
                string json = www.downloadHandler.text;
                Debug.Log(json);
                // Get the texture data from the json string
                var textureData = JsonUtility.FromJson<TextureData>(json);
                // Decode the base64-encoded texture data
                var imageData = Convert.FromBase64String(textureData.texture);
                // Check for and remove extra padding characters
                int padding = imageData.Length % 4;
                if (padding != 0)
                {
                    padding = 4 - padding;
                    Array.Resize(ref imageData, imageData.Length + padding);
                }
                // Create a texture from the image data and assign it to your chosen plane

                if (myPlane.GetComponent<Renderer>().material.mainTexture != null)
                {
                    // If so, destroy the previous texture
                //    Destroy(myPlane.GetComponent<Renderer>().material.mainTexture);
                }

                if (myPlane2.GetComponent<Renderer>().material.mainTexture != null)
                {
                    // If so, destroy the previous texture
                  //  Destroy(myPlane2.GetComponent<Renderer>().material.mainTexture);
                }

                if (myPlane3.GetComponent<Renderer>().material.mainTexture != null)
                {
                    // If so, destroy the previous texture
                  //  Destroy(myPlane3.GetComponent<Renderer>().material.mainTexture);
                }

                if (myPlane4.GetComponent<Renderer>().material.mainTexture != null)
                {
                    // If so, destroy the previous texture
                   // Destroy(myPlane4.GetComponent<Renderer>().material.mainTexture);
                }

                var texture = new Texture2D(1, 1);
                texture.LoadImage(imageData);
                myPlane.GetComponent<Renderer>().material.mainTexture = texture;
                myPlane2.GetComponent<Renderer>().material.mainTexture = texture;
                myPlane3.GetComponent<Renderer>().material.mainTexture = texture;
                myPlane4.GetComponent<Renderer>().material.mainTexture = texture;

            }
        }

        // Define a class to hold the texture data
        [System.Serializable]
        public class TextureData
        {
            public string texture;
        }




    }
}
