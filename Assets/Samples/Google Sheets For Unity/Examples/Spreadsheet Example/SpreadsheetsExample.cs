﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleSheetsForUnity;
using UnityEditor;
using System.Linq;
using System;

public class SpreadsheetsExample : MonoBehaviour
{
    public GameObject ImageParent;
    public ScrollRect scrollView;
    public RawImage Steve;
    public AudioSource audio1;
    public Text chatbox;
    private string _spreadsheetId = "1yEgAnnNCW3n9LcrP4RKab95eQe2DSc5H-cd8CacKiYQ"; // Replace with your spreadsheet ID.
    public string _tableName = "Sheet1"; // Replace with your table name.
    private List<ChatMessage> messages = new List<ChatMessage>();

    [System.Serializable]
    public struct ChatMessage
    {
        
        public string country;
        public string system;
        public string user;
        public string assistant;
        public string flag;
        /*
        public string flagF1;
        public string flagG1;
        public string flagH1;
        public string flagI1;
        public string flagJ1;
        public string flagK1;
        public string flagL1;
        public string flagM1;
        public string flagN1;
        public string flag01;
        public string flagP1;
        public string flagQ1;
        public string flagR1;
        public string flagS1;
        public string flagT1;
        public string flagV1;
        public string flagW1;
        public string flagX1;
        public string flagY1;
        public string flagZ1;
        public string flagAA1;
        public string flagAB1;
        public string flagAC1;
        public string flagAD1;
        public string flagAE1;

        */
    }

    [SerializeField]
    private ChatMessage _chatData = new ChatMessage();

    public Dropdown countryDropdown;
    public Dropdown systemDropdown;

    private List<string> countryList = new List<string>();
    private List<string> systemList = new List<string>();
    private int selectedCountryIndex = 0;
    private int selectedSystemIndex = 0;
    private string selectedCountry = "";
    private string selectedSystem = "";

    private void OnEnable()
    {
        // Subscribe for catching cloud responses.
        Drive.responseCallback += HandleDriveResponse;
    }

    private void OnDisable()
    {
        // Remove listeners.
        Drive.responseCallback -= HandleDriveResponse;
    }

    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 800, 1000));
        GUILayout.BeginHorizontal();
        GUILayout.Space(10f);
        GUILayout.BeginVertical();

        GUILayout.Label("This example uses the Google Sheets for Unity API to work with data on spreadsheets, in a database-like approach.\n" +
            "You can create tables (worksheets) and objects (rows), update them, and retrieve them (one by one, or all at once).",
            GUILayout.MaxWidth(800f));

        GUILayout.Space(10f);
        GUILayout.BeginVertical("Example 'Chat' Object Data:", GUI.skin.box, GUILayout.MaxWidth(230));
        GUILayout.Space(20f);

        GUILayout.BeginHorizontal();
        GUILayout.Space(10f);
        GUILayout.Label("Country:", GUILayout.MaxWidth(100f));
        _chatData.country = GUILayout.TextField(_chatData.country, GUILayout.MaxWidth(100f));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10f);
        GUILayout.Label("System:", GUILayout.MaxWidth(100f));
        _chatData.system = GUILayout.TextField(_chatData.system, GUILayout.MaxWidth(100f));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10f);
        GUILayout.Label("User Message:", GUILayout.MaxWidth(100f));
        _chatData.user = GUILayout.TextField(_chatData.user, GUILayout.MaxWidth(100f));
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(10f);
        GUILayout.Label("Assistant Reply:", GUILayout.MaxWidth(100f));
        _chatData.assistant = GUILayout.TextField(_chatData.assistant, GUILayout.MaxWidth(100f));
        GUILayout.EndHorizontal();

        GUILayout.Space(5f);
        GUILayout.EndVertical();

        GUILayout.Space(10f);

        if (GUILayout.Button("Create Table", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            CreateChatTable();

        if (GUILayout.Button("Save Chat", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            SaveChat();

        if (GUILayout.Button("Retrieve Chat by Country and System", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            RetrieveChat();

        if (GUILayout.Button("Retrieve All Chat", GUILayout.MinHeight(20f), GUILayout.MaxWidth(220f)))
            GetAllChat();

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }


    private void OnCountryDropdownValueChanged(int value)
    {
        selectedCountry = countryList[value];
    }

    private void OnSystemDropdownValueChanged(int value)
    {
        selectedSystem = systemList[value];
    }

    private void CreateChatTable()
    {
        Debug.Log("<color=yellow>Creating a table in the cloud for chat data.</color>");
        // Creating a string array for field names (table headers).
        string[] fieldNames = new string[5];
        fieldNames[0] = "country";
        fieldNames[1] = "system";
        fieldNames[2] = "user";
        fieldNames[3] = "assistant";
        fieldNames[4] = "flag"; // Request for the table to be created on the cloud.
        Drive.CreateTable(fieldNames, _tableName, true);
    }

    private void SaveChat()
    {
        // Set the country and system fields in the ChatMessage object.
        _chatData.country = selectedCountry;
        _chatData.system = selectedSystem; // Get the json string of the object.
        string jsonChat = JsonUtility.ToJson(_chatData);
        Debug.Log("<color=yellow>Sending following chat message to the cloud: \n</color>" + jsonChat);

        // Save the object on the cloud, in a table called like the object type.
        Drive.CreateObject(jsonChat, _tableName, true);
    }

    private void RetrieveChat()
    {
        Debug.Log("<color=yellow>Retrieving chat data from the Cloud for country " + selectedCountry + " and system " + selectedSystem + ".</color>");



        // Retrieve all objects from the table.
        //Drive.GetTable(_tableName, true);



    }

    private void GetAllChat()
    {
        Debug.Log("<color=yellow>Retrieving all chat data from the Cloud.</color>");

        // Get all objects from table 'ChatMessage'.
        Drive.GetTable(_tableName, true);
    }

    // Processes the data received from the cloud.
    public void HandleDriveResponse(Drive.DataContainer dataContainer)
    {
        Debug.Log(dataContainer.msg);
        

        // Check if the type is correct.
        if (string.Compare(dataContainer.objType, _tableName) == 0)
        {
            // Parse from json to the desired object type.
            ChatMessage[] chats = JsonHelper.ArrayFromJson<ChatMessage>(dataContainer.payload);

            // Output some info to check the result.
            Debug.Log("Retrieved " + chats.Length + " chat messages from the cloud.");

            // Create a new parent game object to hold the RawImages
            GameObject imagesParent = new GameObject("ImagesParent", typeof(RectTransform));

            // Calculate the size of each RawImage
            float imageWidth = scrollView.content.rect.width / 2f;
            float imageHeight = imageWidth;

            // Calculate the spacing between the RawImages
            float spacing = 200f;


            


;            // Position the RawImages in rows and columns
            for (int i = 0; i < chats.Length; i++)
            {

                // Decode the base64 string to a texture
               
                byte[] imageBytes = Convert.FromBase64String(chats[i].flag);
                Texture2D texture = new Texture2D(1, 1);
                texture.LoadImage(imageBytes);

                // Create a new RawImage for the texture
                RawImage rawImage = new GameObject("RawImage" + i).AddComponent<RawImage>();
                rawImage.transform.SetParent(imagesParent.transform); // Set the parent of the RawImage
                rawImage.texture = texture; // Set the texture of the RawImage

                // Calculate the row and column of the RawImage
                int row = i / 2;
                int col = i % 2;

                // Set the anchored position of the RawImage based on the row and column
                float x = col * (imageWidth + spacing);
                float y = -row * (imageHeight + spacing);
                rawImage.rectTransform.anchoredPosition = new Vector2(x - 140, y + 200);

                // Set the size of the RawImage
                rawImage.rectTransform.sizeDelta = new Vector2(imageWidth + 150, imageHeight + 150);
            }

            // Set the size of the parent object based on the number of rows and columns
            int numRows = (int)Mathf.Ceil(chats.Length / 2f);
            float parentHeight = numRows * (imageHeight + spacing) - spacing;
            imagesParent.GetComponent<RectTransform>().sizeDelta = new Vector2(scrollView.content.rect.width, parentHeight);

            // Set the parent of the parent object to the scroll view's content object
            imagesParent.transform.SetParent(scrollView.content.transform);

            // Set the position of the parent object
            imagesParent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);

            // Clear the dropdown options
            countryDropdown.ClearOptions();
            systemDropdown.ClearOptions();

            // Update the country and system lists
            countryList.Clear();
            systemList.Clear();
            foreach (ChatMessage chat in chats)
            {
                if (!countryList.Contains(chat.country))
                {
                    countryList.Add(chat.country);
                }

                if (!systemList.Contains(chat.system))
                {
                    systemList.Add(chat.system);
                }
                Debug.Log(chat.user);
                Debug.Log(chat.assistant);
                Debug.Log(chat.flag);
                Debug.Log(chat.assistant);
                //Debug.Log(chat.flagF1);
            }

            // Update the dropdowns with the new lists.
            countryDropdown.AddOptions(countryList);
            countryDropdown.onValueChanged.AddListener(delegate { OnCountryDropdownValueChanged(countryDropdown.value); });

            systemDropdown.AddOptions(systemList);
            systemDropdown.onValueChanged.AddListener(delegate { OnSystemDropdownValueChanged(systemDropdown.value); });
        }



    }
}

