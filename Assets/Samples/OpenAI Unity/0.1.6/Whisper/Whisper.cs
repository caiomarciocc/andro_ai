﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


using Microphone = FrostweepGames.MicrophonePro.Microphone;

namespace OpenAI
{
    [RequireComponent(typeof(AudioSource))]
    public class Whisper : MonoBehaviour
    {


        public GameObject gptObject;


        private AudioClip _workingClip;

        public Text permissionStatusText;

        public Text recordingStatusText;

        public Dropdown devicesDropdown;


        public AudioSource audioSource;

        public Button startRecordButton,
                      stopRecordButton,
                      playRecordedAudioButton,
                      requestPermissionButton,
                      refreshDevicesButton;

        public List<AudioClip> recordedClips;

        public int frequency = 44100;

        public int recordingTime = 120;

        [FrostweepGames.Plugins.ReadOnly]
        public string selectedDevice;

        [FrostweepGames.Plugins.ReadOnly]
        public bool permissionGranted;

        private void Start()
        {
            audioSource = GetComponent<AudioSource>();

            startRecordButton.onClick.AddListener(StartRecord);
            stopRecordButton.onClick.AddListener(StopRecord);
            playRecordedAudioButton.onClick.AddListener(PlayRecordedAudio);
            requestPermissionButton.onClick.AddListener(RequestPermission);
            refreshDevicesButton.onClick.AddListener(RefreshMicrophoneDevicesButtonOnclickHandler);

            devicesDropdown.onValueChanged.AddListener(DevicesDropdownValueChangedHandler);

            selectedDevice = string.Empty;

            Microphone.RecordStreamDataEvent += RecordStreamDataEventHandler;
            Microphone.PermissionChangedEvent += PermissionChangedEvent;

            // no need to request permission in webgl. it does automatically
            requestPermissionButton.interactable = Application.platform != RuntimePlatform.WebGLPlayer;

            recordButton.onClick.AddListener(StartRecording);
        }

        private void OnDestroy()
        {
            Microphone.RecordStreamDataEvent -= RecordStreamDataEventHandler;
            Microphone.PermissionChangedEvent -= PermissionChangedEvent;
        }

        private void Update()
        {
            permissionStatusText.text = $"Microphone permission for device: '{selectedDevice}' is '{(permissionGranted ? "<color=green>granted</color>" : "<color=red>denined</color>")}'";
            recordingStatusText.text = $"Recording status is '{(Microphone.IsRecording(selectedDevice) ? "<color=green>recording</color>" : "<color=yellow>idle</color>")}'";

            if (isRecording)
            {
                time += Time.deltaTime;
                progressBar.fillAmount = time / duration;

                if (time >= duration)
                {
                    time = 0;
                    isRecording = false;
                    EndRecording();
                }
            }
        }

        /// <summary>
        /// Works only in WebGL
        /// </summary>
        /// <param name="samples"></param>
        private void RecordStreamDataEventHandler(Microphone.StreamData streamData)
        {
            // handle streaming recording data
        }

        private void PermissionChangedEvent(bool granted)
        {
            // handle current permission status

            if (permissionGranted != granted)
                RefreshMicrophoneDevicesButtonOnclickHandler();

            permissionGranted = granted;

            Debug.Log($"Permission state changed on: {granted}");
        }

        private void RefreshMicrophoneDevicesButtonOnclickHandler()
        {
            devicesDropdown.ClearOptions();
            devicesDropdown.AddOptions(Microphone.devices.ToList());
            DevicesDropdownValueChangedHandler(0);
        }

        private void RequestPermission()
        {
            Microphone.RequestPermission();
        }

        private void StartRecord()
        {
            _workingClip = Microphone.Start(selectedDevice, false, recordingTime, frequency);
        }

        private void StopRecord()
        {
            Microphone.End(selectedDevice);

            PlayRecordedAudio();
        }

        private void PlayRecordedAudio()
        {
            if (_workingClip == null)
                return;

            audioSource.clip = _workingClip;
            audioSource.Play();

            Debug.Log("start playing");
        }

        private void DevicesDropdownValueChangedHandler(int index)
        {
            if (index < Microphone.devices.Length)
            {
                selectedDevice = Microphone.devices[index];
            }
        }




        [SerializeField] private Button recordButton;
        [SerializeField] private Image progressBar;
        [SerializeField] private Text message;
        [SerializeField] private Dropdown dropdown;
        [SerializeField] private InputField chatInput;

        private readonly string fileName = "output.wav";
        private readonly int duration = 8;
        
        private AudioClip clip;
        private bool isRecording;
        private float time;
        private OpenAIApi openai = new OpenAIApi();

        private void Start2()
        {
            foreach (var device in Microphone.devices)
            {
                dropdown.options.Add(new Dropdown.OptionData(device));
            }
            recordButton.onClick.AddListener(StartRecording);
          //  dropdown.onValueChanged.AddListener(ChangeMicrophone);
            
           // var index = PlayerPrefs.GetInt("user-mic-device-index");
            // dropdown.SetValueWithoutNotify(index);
        }

        private void ChangeMicrophone(int index)
        {
           // PlayerPrefs.SetInt("user-mic-device-index", index);
        }

        public void triggerRecording()

        {
            StartRecording();
        }
        
        private void StartRecording()
        {
            isRecording = true;
            recordButton.enabled = false;

           // var index = PlayerPrefs.GetInt("user-mic-device-index");
            clip = Microphone.Start(selectedDevice, false, duration, 44100);
        }

        private async void EndRecording()
        {
            message.text = "Transcripting...";
            
            Microphone.End(null);
            byte[] data = SaveWav.Save(fileName, clip);
            
            var req = new CreateAudioTranscriptionsRequest
            {
                FileData = new FileData() {Data = data, Name = "audio.wav"},
                // File = Application.persistentDataPath + "/" + fileName,
                Model = "whisper-1",
                Language = "en"
            };
            var res = await openai.CreateAudioTranscription(req);

            progressBar.fillAmount = 0;
            message.text = res.Text;
            recordButton.enabled = true;
            chatInput.text = res.Text;
          gptObject.SendMessage("SendReply");
        }

        private void Update2()
        {
            if (isRecording)
            {
                time += Time.deltaTime;
                progressBar.fillAmount = time / duration;
                
                if (time >= duration)
                {
                    time = 0;
                    isRecording = false;
                    EndRecording();
                }
            }
        }
    }
}
