using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class APITextRequest : MonoBehaviour
{
    public string apiURL = "https://963knightmoon369.readyplayer.me/api/texture?prompt=abstract%20colorful%20seamless%20synthwave";

    IEnumerator Start()
    {
        using (UnityWebRequest www = UnityWebRequest.Get(apiURL))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }
}
