using System;

namespace MHLab.ReactUI.Core
{
    [Serializable]
    public struct UIEventHeader
    {
        public string eventName;

        public UIEventHeader(string eventName)
        {
            this.eventName = eventName;
        }
    }
}