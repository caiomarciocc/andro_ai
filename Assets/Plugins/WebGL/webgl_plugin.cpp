#include <emscripten.h>
#include <stdlib.h>
#include <stdio.h>

extern "C" {
    EMSCRIPTEN_KEEPALIVE
    void ReceiveAudioData(unsigned char* data, int length)
    {
        // Process the audio data
        // ...

        printf("Received audio data of length %d\n", length);
    }
}
