using System;
using System.Web;

namespace Microsoft.CognitiveServices.Speech
{
    public class ArrayBufferEventArgs : EventArgs
    {
        private ArraySegment<byte> _payload;

        public ArrayBufferEventArgs(ArraySegment<byte> payload)
        {
            _payload = payload;
        }

        public ArraySegment<byte> Payload
        {
            get
            {
                return _payload;
            }
        }
    }
}
