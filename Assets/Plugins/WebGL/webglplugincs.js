var LibraryManager = {
  library: {}
};

if (typeof mergeInto !== 'function') {
    function mergeInto(dst, src) {
        for (var k in src) {
            if (!src.hasOwnProperty(k))
                continue;
            dst[k] = src[k];
        }
        return dst;
    }
}

mergeInto(LibraryManager.library, {
  // Function to enable audio source
  EnableAudioSource: function () {
    var audioReceiver = Module.mono_find_root_managed_object("AudioReceiver");
    audioReceiver.InvokeMethod("EnableAudioSource");
  },

  // Function to disable audio source
  DisableAudioSource: function () {
    var audioReceiver = Module.mono_find_root_managed_object("AudioReceiver");
    audioReceiver.InvokeMethod("DisableAudioSource");
  },

  // Function to receive audio data
  ReceiveAudioData: function (buffer, length) {
    var audioReceiver = Module.mono_find_root_managed_object("AudioReceiver");
    var data = new Uint8Array(Module.HEAPU8.buffer, buffer, length);
    audioReceiver.InvokeMethod("OnAudioDataReceived", data);
  },
});
