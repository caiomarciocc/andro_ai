using System;
using System.Runtime.InteropServices;
using UnityEngine;

public class AudioReceiver : MonoBehaviour
{
    private AudioSource audioSource;

    [DllImport("__Internal")]
    private static extern void ReceiveAudioData(IntPtr data, int length);

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void EnableAudioSource()
    {
        audioSource.enabled = true;
    }

    public void DisableAudioSource()
    {
        audioSource.enabled = false;
    }

    public void OnAudioDataReceived(byte[] data)
    {
        AudioClip audioClip = WavUtility.ToAudioClip(data);
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    public void SendAudioData(byte[] data)
    {
        GCHandle handle = GCHandle.Alloc(data, GCHandleType.Pinned);
        ReceiveAudioData(Marshal.UnsafeAddrOfPinnedArrayElement(data, 0), data.Length);
        handle.Free();
    }
}
