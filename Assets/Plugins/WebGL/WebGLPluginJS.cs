﻿using System;
using System.Runtime.InteropServices;

/// <summary>
/// Class with a JS Plugin functions for WebGL.
/// </summary>
public static class WebGLPluginJS
{




#if !UNITY_EDITOR && UNITY_WEBGL

   


    
#endif

    [DllImport("__Internal")]
    private static extern void SetupRpm(string partner);

    [DllImport("__Internal")]
    private static extern void ShowReadyPlayerMeFrame();

    [DllImport("__Internal")]
    private static extern void HideReadyPlayerMeFrame();


    // Importing "CallFunction"
    [DllImport("__Internal")]
    public static extern void CallFunction();

    // Importing "PassTextParam"
    [DllImport("__Internal")]
    public static extern void PassTextParam(string text);

    // Importing "PassNumberParam"
    [DllImport("__Internal")]
    public static extern void PassNumberParam(int number);

    // Importing "GetTextValue"
    [DllImport("__Internal")]
    public static extern string GetTextValue();

    internal static object GetTextValueBytes()
    {
        throw new NotImplementedException();
    }

    // Importing "GetNumberValue"
    [DllImport("__Internal")]
    public static extern int GetNumberValue();

    public static void SetIFrameVisibility(bool isVisible)
    {

        if (isVisible)
        {

            ShowReadyPlayerMeFrame();
            return;
        }

        HideReadyPlayerMeFrame();

    }
    public static void SetupRpmFrame(string partner)
    {
        SetupRpm(partner);

    }
}
