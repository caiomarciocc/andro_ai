 // Define text value
      var textToPass2 = "You got this text from the plugin 222";

mergeInto(LibraryManager.library, {

    

// Function example
   CallFunction: function () {
      // Show a message as an alert
      window.alert("You called a function from this plugin!");
   },

   // Function with the text param
   PassTextParam: function (text) {
      // Convert bytes to the text
      var convertedText = Pointer_stringify(text);

      // Show a message as an alert
      window.alert("You've passed the text: " + convertedText);
   },

   // Function with the number param
   PassNumberParam: function (number) {
      // Show a message as an alert
      window.alert("The number is: " + number);
   },

   // Function returning text value
   GetTextValue: function () {
      // Define text value
      var textToPass = "You got this text from the plugin";

      // Create a buffer to convert text to bytes
      var bufferSize = lengthBytesUTF8(textToPass) + 1;
      var buffer = _malloc(bufferSize);

      // Convert text
      stringToUTF8(textToPass, buffer, bufferSize);

      // Return text value
      return buffer;
   },


 // Function returning text value
   GetTextValue2: function () {
     

      // Create a buffer to convert text to bytes
      var bufferSize = lengthBytesUTF8(textToPass) + 1;
      var buffer = _malloc(bufferSize);

      // Convert text
      stringToUTF8(textToPass2, buffer, bufferSize);

      // Return text value
      return buffer;
   },


   // Function returning number value
   GetNumberValue: function () {
      // Return number value
      return 2020;
   },

    ShowReadyPlayerMeFrame: function () {
        var rpmContainer = document.getElementById("rpm-container");
        rpmContainer.style.display = "block";
    },
  
    HideReadyPlayerMeFrame: function () {
        var rpmContainer = document.getElementById("rpm-container");
        rpmContainer.style.display = "none";
    },
        
    SetupRpm: function (partner){
        setupRpmFrame(UTF8ToString(partner));
    },


// Function to call updateVoiceListButton click event listener function
   CallVoiceListButton: function () {
      // Trigger the click event on the downloadButton element
      updateVoiceListButton.dispatchEvent(new Event('click'));
   },




   // Function to call downloadButton click event listener function
   CallscenarioStartButton: function () {
      // Trigger the click event on the downloadButton element
      scenarioStartButton.dispatchEvent(new Event('click'));
   }
});