using System;
using UnityEngine;

namespace ReadyPlayerMe
{
    public class AvatarLoader
    {

        public SkinnedMeshRenderer skinMeshRPM;
        public SkinnedMeshRenderer skinMeshUnity;
      //      public Avatar rpmAvatar = Resources.Load<Avatar>("Fem");
       // public Animator rpmAnimator = GameObject.Find("H33").GetComponent<Animator>();
        

        private const string TAG = nameof(AvatarLoader);

        /// Called upon avatar loader failure.
        public event EventHandler<FailureEventArgs> OnFailed;

        /// Called upon avatar loader progress change.
        public event EventHandler<ProgressChangeEventArgs> OnProgressChanged;

        /// Called upon avatar loader success.
        public event EventHandler<CompletionEventArgs> OnCompleted;

        /// Avatar Importer instance used for importing the GLB model.
        public IAvatarImporter AvatarImporter { get; set; }

        /// If true, saves the avatar in the Asset folder.
        public bool SaveInProjectFolder { get; set; }

        /// Set the timeout for download requests 
        public int Timeout { get; set; } = 20;

        /// Scriptable Object Avatar API request parameters configuration
        public AvatarConfig AvatarConfig;

        private bool avatarCachingEnabled;

        private OperationExecutor<AvatarContext> executor;
        private string avatarUrl;
        private float startTime;

        public AvatarLoader()
        {
            var loaderSettings = Resources.Load<AvatarLoaderSettings>(AvatarLoaderSettings.RESOURCE_PATH);
            avatarCachingEnabled = loaderSettings && loaderSettings.AvatarCachingEnabled;
            AvatarConfig = loaderSettings ? loaderSettings.AvatarConfig : null;
        }

        /// Load avatar from given url
        public void LoadAvatar(string url)
        {
            startTime = Time.timeSinceLevelLoad;
            SDKLogger.Log(TAG, $"Started loading avatar with config {(AvatarConfig ? AvatarConfig.name : "None")} from URL {url}");
            avatarUrl = url;
            Load(url);
        }

        /// Cancel avatar loading
        public void Cancel()
        {
            executor.Cancel();
        }

        private async void Load(string url)
        {
            var context = new AvatarContext();
            context.Url = url;
            context.SaveInProjectFolder = SaveInProjectFolder;
            context.AvatarCachingEnabled = avatarCachingEnabled;
            context.AvatarConfig = AvatarConfig;
            context.ParametersHash = AvatarCache.GetAvatarConfigurationHash(AvatarConfig);

            executor = new OperationExecutor<AvatarContext>(new IOperation<AvatarContext>[]
            {
                new UrlProcessor(),
                new MetadataDownloader(),
                new AvatarDownloader(),
                AvatarImporter ?? new GltfUtilityAvatarImporter(),
                new AvatarProcessor()
            });
            executor.ProgressChanged += ProgressChanged;
            executor.Timeout = Timeout;

            ProgressChanged(0, nameof(AvatarLoader));
            try
            {
                context = await executor.Execute(context);
            }
            catch (CustomException exception)
            {
                Failed(exception.FailureType, exception.Message);
                return;
            }

            if (executor.IsCancelled)
            {
                SDKLogger.Log(TAG, $"Avatar loading cancelled");
            }
            else
            {
                var avatar = (GameObject)context.Data;

                avatar.SetActive(true);
                OnCompleted?.Invoke(this, new CompletionEventArgs
                {
                    Avatar = avatar,
                    Url = context.Url,
                    Metadata = context.Metadata

                }
                   

            



                );

                if (context.Metadata.BodyType == BodyType.HalfBody)
                {
                    Debug.Log("Avatar = " + avatar + " URL = " + context.Url + " Metadata: " + context.Metadata);

                    skinMeshUnity = GameObject.Find("Renderer_Avatar_Half").GetComponent<SkinnedMeshRenderer>();
                    skinMeshRPM = GameObject.Find("Renderer_Avatar").GetComponent<SkinnedMeshRenderer>();
                    skinMeshUnity.sharedMesh = skinMeshRPM.sharedMesh;
                    //skinMeshUnity.rootBone = skinMeshRPM.rootBone;
                    //skinMeshUnity.bones = skinMeshRPM.bones;
                    skinMeshUnity.materials = skinMeshRPM.materials;
                    Debug.Log(skinMeshUnity.materials);
                    skinMeshRPM.materials = new Material[0];

                    // GameObject.Find("Wolf3D_Avatar_Me").SetActive(false);
                    // GameObject.Find("Renderer_Avatar_Half").SetActive(true);
                    //  GameObject.Find("Wolf3D_Avatar_Fem").SetActive(false);

                    //CopySkinnedMeshRendererValues(skinMeshRPM, skinMeshUnity);


                    GameObject.Find("Wolf3D_Avatar_Me").GetComponent<SkinnedMeshRenderer>().enabled = false;
                    GameObject.Find("Wolf3D_Avatar_Fem").GetComponent<SkinnedMeshRenderer>().enabled = false;
                    GameObject.Find("Renderer_Avatar_Half").GetComponent<SkinnedMeshRenderer>().enabled = true;

                   // GameObject.Find("Wolf3D_Avatar_Me").layer = LayerMask.NameToLayer("ignore 3d");
                   // GameObject.Find("Wolf3D_Avatar_Fem").layer = LayerMask.NameToLayer("ignore 3d");
                   // GameObject.Find("Renderer_Avatar_Half").layer = LayerMask.NameToLayer("Head 3d");

                    Debug.Log("hiiiiiii");


                }

                if (context.Metadata.BodyType == BodyType.FullBody)
                {
                    if (context.Metadata.OutfitGender == OutfitGender.Masculine)
                    {
                        skinMeshUnity = GameObject.Find("Wolf3D_Avatar_Me").GetComponent<SkinnedMeshRenderer>();
                        skinMeshRPM = GameObject.Find("Renderer_Avatar").GetComponent<SkinnedMeshRenderer>();
                        skinMeshUnity.sharedMesh = skinMeshRPM.sharedMesh;
                      // skinMeshUnity.rootBone = skinMeshRPM.rootBone;
                      //  skinMeshUnity.bones = skinMeshRPM.bones;
                        skinMeshUnity.materials = skinMeshRPM.materials;
                        Debug.Log(skinMeshUnity.materials);
                        skinMeshRPM.materials = new Material[0];


                        //GameObject.Find("Wolf3D_Avatar").SetActive(true);
                         //GameObject.Find("Renderer_Avatar_Half").SetActive(false);
                         //GameObject.Find("Wolf3D_Avatar_Fem").SetActive(false);

                        //CopySkinnedMeshRendererValues(skinMeshRPM, skinMeshUnity);


                        GameObject.Find("Wolf3D_Avatar_Me").GetComponent<SkinnedMeshRenderer>().enabled = true;
                        //GameObject.Find("Wolf3D_Avatar_Fem").GetComponent<SkinnedMeshRenderer>().enabled = false;
                       //GameObject.Find("Renderer_Avatar_Half").GetComponent<SkinnedMeshRenderer>().enabled = false;


                        GameObject.Find("Wolf3D_Avatar_Me").layer = default;
                       // GameObject.Find("Wolf3D_Avatar_Fem").layer = LayerMask.NameToLayer("ignore 3d");
                       // GameObject.Find("Renderer_Avatar_Half").layer = LayerMask.NameToLayer("ignore 3d");

                        Debug.Log("hiiiiiii");


                     
                    }



                    else if (context.Metadata.OutfitGender == OutfitGender.Feminine)
                    {
                        
                        skinMeshUnity = GameObject.Find("Wolf3D_Avatar_Me").GetComponent<SkinnedMeshRenderer>();
                        skinMeshRPM = GameObject.Find("Renderer_Avatar").GetComponent<SkinnedMeshRenderer>();
                        skinMeshUnity.sharedMesh = skinMeshRPM.sharedMesh;
                        Debug.Log("hey");
                        //skinMeshUnity.rootBone = skinMeshRPM.rootBone;
                        //skinMeshUnity.bones = skinMeshRPM.bones;
                        skinMeshUnity.materials = skinMeshRPM.materials;
                        Debug.Log(skinMeshUnity.materials);
                        skinMeshRPM.materials = new Material[0];
                       // GameObject.Find("Wolf3D_Avatar_Me").SetActive(false);
                       // GameObject.Find("Renderer_Avatar_Half").SetActive(false);
                        //GameObject.Find("Wolf3D_Avatar_Fem").SetActive(true);             
                        //CopySkinnedMeshRendererValues(skinMeshRPM, skinMeshUnity);

                       GameObject.Find("Wolf3D_Avatar_Me").GetComponent<SkinnedMeshRenderer>().enabled = false;
                        GameObject.Find("Wolf3D_Avatar_Fem").GetComponent<SkinnedMeshRenderer>().enabled = true;
                        GameObject.Find("Renderer_Avatar_Half").GetComponent<SkinnedMeshRenderer>().enabled = false;
                        GameObject.Find("Renderer_Avatar").GetComponent<SkinnedMeshRenderer>().enabled = false;

                        //skinMeshRPM.enabled = false;

                        // rpmAnimator.avatar = rpmAvatar;


                       // GameObject.Find("Wolf3D_Avatar_Me").layer = LayerMask.NameToLayer("Me 3d");
                       // GameObject.Find("Wolf3D_Avatar_Fem").layer = LayerMask.NameToLayer("Fem 3d");
                       // GameObject.Find("Renderer_Avatar_Half").layer = LayerMask.NameToLayer("Head 3d");
                        Camera maincamera = GameObject.Find("Main Camera").GetComponent<Camera>();
                        
                       // GameObject.Find("Main Camera").GetComponent<Camera>().cullingMask &= ~(1 << LayerMask.NameToLayer("Head 3d"));
                      //  GameObject.Find("Main Camera").GetComponent<Camera>().cullingMask &= ~(1 << LayerMask.NameToLayer("Me 3d"));
                       // GameObject.Find("Main Camera").GetComponent<Camera>().cullingMask |= (1 << LayerMask.NameToLayer("Fem 3d"));

                        // Remove the "Enemies" layer from the culling mask
                      

                        Debug.Log("hiiiiiii");
                    }



                }

               

                SDKLogger.Log(TAG, $"Avatar loaded in {Time.timeSinceLevelLoad - startTime:F2} seconds.");
                Debug.Log("sup");
              
               



            }
        }


        




        private void ProgressChanged(float progress, string type)
        {
            OnProgressChanged?.Invoke(this, new ProgressChangeEventArgs
            {
                Operation = type,
                Url = avatarUrl,
                Progress = progress
            });
        }

        // TODO: add the messages here
        private void Failed(FailureType type, string message)
        {
            OnFailed?.Invoke(this, new FailureEventArgs
            {
                Type = type,
                Url = avatarUrl,
                Message = message
            });
            SDKLogger.Log(TAG, $"Failed to load avatar. Error type {type}. URL {avatarUrl}. Message {message}");
        }
    }
}
