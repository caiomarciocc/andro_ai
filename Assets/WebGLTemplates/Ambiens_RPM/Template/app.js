function setupRpmFrame(subdomain) {
    let rpmFrame = document.getAnimations('rpm-frame');
    rpmFrame.src = `https://${subdomain !== "" ? subdomain : "demo"}.readyplayer.me/avatar?frameApi`
    window.addEventListener("message", subscribe);
    document.addEventListener("message", subscribe);
    function subscribe(event) {
        const json = parse(event);
        if (
            unityGame == null ||
            json?.source !== "readyplayerme" ||
            json?.eventName == null
        ) {
            return;
        }
        unityGame.SendMessage(
            "DebugPanel",
            "LogMessage",
            `Event: ${json.eventName}`
        );

        // Susbribe to all events sent from Ready Player Me once frame is ready
        if (json.eventName === "v1.frame.ready") {
            rpmFrame.contentWindow.postMessage(
                JSON.stringify({
                    target: "readyplayerme",
                    type: "subscribe",
                    eventName: "v1.**",
                }),
                "*"
            );
        }

        // Get avatar GLB URL
        if (json.eventName === "v1.avatar.exported") {
            rpmContainer.style.display = "none";
            unityGame.SendMessage(
                "WebAvatarLoader",
                "OnWebViewAvatarGenerated",
                json.data.url
            );
            console.log(`Avatar URL: ${json.data.url}`);
        }

        // Get user id
        if (json.eventName === "v1.user.set") {
            console.log(`User with id ${json.data.id} set: ${JSON.stringify(json)}`);
        }
    }

    function parse(event) {
        try {
            return JSON.parse(event.data);
        } catch (error) {
            return null;
        }
    }
}

function displayRpm() {
    rpmContainer.style.display = "block";
}

function hideRpm() {
    rpmContainer.style.display = "none";
}

function showFullscreen() {
    canvasWrapper.requestFullscreen();
}
(function () {
    


    //UNITY STUFF
    var buildUrl = "Build";
    var loaderUrl = buildUrl + "/BUIDS AI.loader.js";
    var config = {
        dataUrl: buildUrl + "/BUIDS AI.data?c=" + (Math.random() * 999999),
        frameworkUrl: buildUrl + "/BUIDS AI.framework.js",
        codeUrl: buildUrl + "/BUIDS AI.wasm",
        streamingAssetsUrl: "StreamingAssets",
        companyName: "DefaultCompany",
        productName: "UnityExample_WebGL",
        productVersion: "0.2.0",
    };


    



    function iOS() {
        return [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod'
        ].includes(navigator.platform)
        // iPad on iOS 13 detection
        || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
    }
    function isFullscreen(){
        return document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement;
    }
    var main_container = document.querySelector("#main-container");
    var container = document.querySelector("#unity-container");
    var canvas = document.querySelector("#unity-canvas");
    var loader= document.querySelector("#loader");
    var loaderFill= document.querySelector("#fill");
    var toggle_fullscreen=document.querySelector("#toggle_fullscreen");

    function onProgress(progress) {
        loaderFill.style.width = progress * 100 + "%";
    }

    function onComplete(unityInstance) {
        loader.remove();
    }
    var resizeTimeOut;
    function onWindowResize() {
        var width = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;

        var height = window.innerHeight
        || document.documentElement.clientHeight
        || document.body.clientHeight;

        canvas.height=height;
        canvas.width=width;
    }
    function onWindowResizeWithDelay(){
        clearTimeout(resizeTimeOut);
        resizeTimeOut = setTimeout(onWindowResize, 200);
    }


    var script = document.createElement("script");
    script.src = loaderUrl;
    script.onload = () => {
        createUnityInstance(canvas, config, onProgress)
            .then(onComplete)
            .catch((message) => {
                alert(message);
        });
    };
    document.body.appendChild(script);

    window.addEventListener('resize', onWindowResizeWithDelay);
    onWindowResizeWithDelay();


    if(iOS()){
        toggle_fullscreen.style.display="none";
    }
    else{
        toggle_fullscreen.addEventListener('click', function(){

           // if already full screen; exit
            // else go fullscreen
            if ( isFullscreen() ) {
              
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } 
                else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } 
                else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                } 
                else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
                
            } else {
                
                if (main_container.requestFullscreen) {
                    main_container.requestFullscreen();
                } else if (main_container.mozRequestFullScreen) {
                    main_container.mozRequestFullScreen();
                } else if (main_container.webkitRequestFullscreen) {
                    main_container.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                } else if (container.msRequestFullscreen) {
                    main_container.msRequestFullscreen();
                }
            }
           
        });

        document.onfullscreenchange = function ( event ) {
            if ( isFullscreen() ) {
                if (toggle_fullscreen.classList.contains("fullscreenON")) {
                    toggle_fullscreen.classList.remove("fullscreenON");
                }
                toggle_fullscreen.classList.add("fullscreenOFF");

               
            } else {
                
                if (toggle_fullscreen.classList.contains("fullscreenOFF")) {
                    toggle_fullscreen.classList.remove("fullscreenOFF");
                }
                toggle_fullscreen.classList.add("fullscreenON");

              
            }
            setTimeout(() => {
                canvas.width=1000;
                onWindowResizeWithDelay();
            }, 400);
        };

    }

})();
