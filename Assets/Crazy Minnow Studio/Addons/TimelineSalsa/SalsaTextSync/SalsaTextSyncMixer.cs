﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.Playables;

namespace CrazyMinnow.SALSA.Timeline
{
	public class SalsaTextSyncMixer : PlayableBehaviour {

		public override void ProcessFrame(Playable playableHandle, FrameData info, object playerData) {
			var textSyncTrackBinding = playerData as TextSync.SalsaTextSync;
			if (textSyncTrackBinding == null)
				return;

			string text = string.Empty;
			var count = playableHandle.GetInputCount();

			for (var i = 0; i < count; i++) {

				var inputHandle = playableHandle.GetInput(i);

				if (inputHandle.IsValid() && inputHandle.GetPlayState() == PlayState.Playing)
				{
					var data = ((ScriptPlayable<SalsaTextSyncBehavior>) inputHandle).GetBehaviour();
					if (data != null) {
						text = data.textSyncText;
					}

					// we've found an active clip and grabbed the data from the behavior clip -- now use it.
					break;
				}
			}

			// we'll update the CM_TextSync text field for Timeline scrubbing.
			textSyncTrackBinding.text = text;

			// avoid calling the .Say() method multiple times during timeline play. IOW, let CM_TextSync
			// do its job after timeline does it by firing the .Say() method. Also, don't fire this if we're
			// not actually playing.
			if (!textSyncTrackBinding.textSyncIsTalking)
			{
#if UNITY_EDITOR
				if (EditorApplication.isPlaying)
#endif
				textSyncTrackBinding.Say(text);
			}
		}
	}
}
