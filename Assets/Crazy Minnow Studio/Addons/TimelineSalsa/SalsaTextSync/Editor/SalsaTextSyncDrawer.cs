﻿using System;
using UnityEditor;
using UnityEngine;

namespace CrazyMinnow.SALSA.Timeline
{
	[CustomPropertyDrawer(typeof(SalsaTextSyncBehavior))]
	public class SalsaTextSyncDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
		{
			int fieldCount = 1;
			return fieldCount * EditorGUIUtility.singleLineHeight;
		}
		
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			SerializedProperty tsTextProp = property.FindPropertyRelative("textSyncText");

			var singleFieldRect = new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
			EditorGUI.PropertyField(singleFieldRect, tsTextProp);

			// Find the clip this behaviour is associated with
			SalsaTextSyncClip clip = property.serializedObject.targetObject as SalsaTextSyncClip;
			if (!clip) return;
			

			var tsText = clip.template.textSyncText;
			if (clip.template.salsaTextSync == null) return;
			var tsWpm = clip.template.salsaTextSync.wordsPerMinute;

			// Assume that the currently selected object is the internal class UnityEditor.Timeline.EditorClip
			// this gives you access to the clip start, duration etc.
			SerializedObject editorGUI = new SerializedObject(Selection.activeObject);

			// Grab the duration, set new duration
			SerializedProperty duration = editorGUI.FindProperty("m_Clip.m_Duration");
			if (duration == null)
				duration = editorGUI.FindProperty("m_Item.m_Duration");

			if (duration != null)
			{
				duration.doubleValue = (double) (tsText.Split(' ').Length * 60.0f / tsWpm);
				if ((double.IsPositiveInfinity(duration.doubleValue))) return; // handles clearing the editor field without error
			}
			
			// Grab the clip title, set new title
			SerializedProperty title = editorGUI.FindProperty("m_Clip.m_DisplayName");
			if (title == null)
				title = editorGUI.FindProperty("m_Item.m_DisplayName");

			if (title != null)
			{
				title.stringValue = (tsText!="Welcome to SALSA, Simple Automated Lip Sync Approximation.")?tsText:"DEFAULT TEXT";
			}

			editorGUI.ApplyModifiedProperties();
		}
	}
}