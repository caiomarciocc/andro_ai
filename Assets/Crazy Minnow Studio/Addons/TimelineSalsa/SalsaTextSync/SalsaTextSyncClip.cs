﻿using System;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace CrazyMinnow.SALSA.Timeline
{
	[Serializable]
	public class SalsaTextSyncBehavior : PlayableBehaviour {
		public string textSyncText = "Welcome to SALSA, Simple Automated Lip Sync Approximation.";
		public TextSync.SalsaTextSync salsaTextSync;
	}

	[Serializable]
	public class SalsaTextSyncClip : PlayableAsset, ITimelineClipAsset {

		public SalsaTextSyncBehavior template = new SalsaTextSyncBehavior();

		public override Playable CreatePlayable(PlayableGraph graph, GameObject go)
		{
			return ScriptPlayable<SalsaTextSyncBehavior>.Create(graph, template);
		}

		public ClipCaps clipCaps {
			get { return ClipCaps.None; }
		}
	}
}
