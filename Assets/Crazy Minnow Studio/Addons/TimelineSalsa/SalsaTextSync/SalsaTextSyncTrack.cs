﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace CrazyMinnow.SALSA.Timeline
{
	[TrackColor(0.2157f, 0.38039f, 0.68627f)]
	[TrackClipType(typeof(SalsaTextSyncClip))]
	[TrackBindingType(typeof(TextSync.SalsaTextSync))]
	public class SalsaTextSyncTrack : TrackAsset {
		public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount) {
			var director = go.GetComponent<PlayableDirector>();
			if (director != null)
			{
				var boundObject = director.GetGenericBinding(this) as TextSync.SalsaTextSync;
				if (boundObject != null)
				{
					foreach (var clip in GetClips())
					{
						if (clip.asset != null)
						{
							// cast the clip as SalsaTextSyncClip to access its public fields
							var textSyncClip = clip.asset as SalsaTextSyncClip;
							// store reference to the associated CM_TextSync instance for use in the drawer.
							if (textSyncClip)
								textSyncClip.template.salsaTextSync = boundObject;
						}
					}
				}
			}

			return ScriptPlayable<SalsaTextSyncMixer>.Create(graph, inputCount);
		}
	}
}