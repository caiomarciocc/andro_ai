using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TNVirtualKeyboard1 : MonoBehaviour
{
	
	public static TNVirtualKeyboard1 instance;
	
	public string words1 = "";
	
	public GameObject vkCanvas1;
	
	public InputField targetText1;

	
	
	
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
		HideVirtualKeyboard();
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void KeyPress(string k){
		words1 += k;
		targetText1.text = words1;	
	}
	
	public void Del(){
		words1 = words1.Remove(words1.Length - 1, 1);
		targetText1.text = words1;	
	}
	
	public void ShowVirtualKeyboard(){
		vkCanvas1.SetActive(true);
	}
	
	public void HideVirtualKeyboard(){
		vkCanvas1.SetActive(false);
	}
}
